from django.shortcuts import render
from django.http import JsonResponse, QueryDict
from django.views import View
from .models import User
from .forms import CreateUserForm, DeleteUserForm


def email_exists(request):
    return JsonResponse({"exists": User.is_email_exists(request.GET.get("email"))})


def index(request):
    return render(request, "accounts/index.html")


class CreateAccountView(View):
    def get(self, request):
        return render(request, "accounts/create.html", {"form": CreateUserForm()})

    def post(self, request):
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
        return JsonResponse({"success": form.is_valid(), "errors": form.errors.as_json()})


class DeleteAccountView(View):
    def post(self, request):
        form = DeleteUserForm(request.POST)
        success = False
        if form.is_valid():
            data = form.cleaned_data
            success = bool(
                User.objects.filter(email=data["email"], password=data["password"]).delete()[0]
            )
        return JsonResponse({"success": success})


class AccountListView(View):
    def get(self, request):
        return JsonResponse(list(User.objects.values("name", "email")), safe=False)
