from django.test import TestCase
from django.urls import reverse
from .models import User


class AccountsRegisterTestCase(TestCase):
    def setUp(self):
        self.account_data = {"name": "Kak Eswe", "email": "f@sil.com", "password": "akug4nteng"}

    def test_register_page(self):
        response = self.client.get(reverse("create-account"))
        self.assertTemplateUsed(response, "accounts/create.html")

    def test_register(self):
        self.client.post(reverse("create-account"), self.account_data)
        self.assertEqual(User.objects.count(), 1)

    def test_register_email_already_exists(self):
        self.test_register()
        self.client.post(reverse("create-account"), self.account_data)
        self.assertEqual(User.objects.count(), 1)


class DeleteUserTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            name="Pemakai email", email="ud@hdi.pake", password="gakaman"
        )

    def test_delete_account_valid(self):
        response = self.client.post(
            reverse("delete-account"), {"email": self.user.email, "password": self.user.password}
        )
        self.assertEqual(User.objects.count(), 0)
        self.assertJSONEqual(response.content.decode(), {"success": True})

    def test_delete_account_invalid(self):
        response = self.client.post(
            reverse("delete-account"),
            {"email": self.user.email, "password": self.user.password + "a"},
        )
        self.assertEqual(User.objects.count(), 1)
        self.assertJSONEqual(response.content.decode(), {"success": False})


class EmailExistsTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            name="Pemakai email", email="ud@hdi.pake", password="gakaman"
        )

    def test_email_exists_yes(self):
        response = self.client.get(reverse("email-exists"), {"email": self.user.email})
        self.assertJSONEqual(response.content.decode(), {"exists": True})

    def test_email_exists_no(self):
        response = self.client.get(reverse("email-exists"), {"email": self.user.email + "haha"})
        self.assertJSONEqual(response.content.decode(), {"exists": False})


class AccountsListTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            name="Pemakai email", email="ud@hdi.pake", password="gakaman"
        )

    def test_get_accounts_list(self):
        response = self.client.get(reverse("list-account"))
        self.assertJSONEqual(
            response.content.decode(), [{"name": self.user.name, "email": self.user.email}]
        )
