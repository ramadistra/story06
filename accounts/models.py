from django.db import models
from django.core.exceptions import ValidationError


class User(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=32)

    @classmethod
    def is_email_exists(cls, email):
        return cls.objects.filter(email=email).exists()

    @classmethod
    def delete_user(cls, email, password):
        user = cls.objects.filter(email=email, password=password).first()
        if user:
            user.delete()
