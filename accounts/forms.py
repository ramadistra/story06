from django import forms
from .models import User


class CreateUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = "__all__"
        widgets = {"password": forms.PasswordInput()}


class DeleteUserForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(max_length=32)
