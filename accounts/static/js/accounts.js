$(function() {
  function createField(name) {
    var error = $("#errors_" + name);
    var field = $("#id_" + name);
    var validations = [];

    function initialCheck() {
      if (field[0].checkValidity()) return null;
      return field.attr("required") && !field.val()
        ? `Please fill in your ${name}.`
        : `Invalid ${name}.`;
    }

    function validate() {
      var err = initialCheck();
      if (err) {
        error.html(err);
        return false;
      }
      if (validations.length == 0) return true;

      return Promise.all(validations.map(f => f(field[0]))).then(errors => {
        for (err of errors) {
          if (err) {
            error.html(err);
            return false;
          }
        }
        return true;
      });
    }

    field
      .blur(function() {
        if (!field.val()) return;
        validate();
      })
      .on("change paste keyup", function() {
        error.empty();
      });

    return {
      validate: validate,
      addValidation(v) {
        validations.push(v.bind(field));
      }
    };
  }

  var fields = ["name", "password", "email"].map(createField);
  fields[2].addValidation(elem =>
    $.ajax({
      type: "GET",
      url: "/accounts/email-exists?" + $.param({ email: elem.value })
    }).then(data =>
      data.exists ? "An account with this e-mail has been registered." : null
    )
  );

  $(".Form__button").click(function(e) {
    e.preventDefault();
    Promise.all(fields.map(field => field.validate()))
      .then(
        validations =>
          validations.every(a => a) &&
          $.ajax({
            type: "POST",
            url: "/accounts/create",
            data: $(".Form").serialize()
          })
      )
      .then(data => {
        if (!data.success) return;
        $(".success").removeClass("hidden");
        setTimeout(() => $(".success").addClass("hidden"), 1500);
      });
  });
});
