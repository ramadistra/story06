function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

function unsubscribe(email) {
  var password = prompt("Masukkan password untuk " + email);
  if (!password) return;
  fetch("/accounts/delete", {
    credentials: "include",
    method: "POST",
    mode: "same-origin",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded",
      "X-CSRFToken": getCookie("csrftoken")
    },
    body: $.param({ email: email, password: password })
  })
    .then(response => response.json())
    .then(data => {
      alert(data.success ? "See you soon :(" : "Invalid credentials.");
    });
}

window.addEventListener("load", function() {
  function fetchSubcribers() {
    fetch("/accounts/list")
      .then(response => response.json())
      .then(data =>
        (data || []).map(
          user => `
                    <tr>
                        <td>${user.name}</td>
                        <td>${user.email}</td>
                        <td><button onclick="unsubscribe('${
                          user.email
                        }')" class="unsub-button">Unsubscribe</button></td>
                    </tr>`
        )
      )
      .then(content => {
        if (content.length < 1) {
          table.innerHTML = "There are no subscribers right. Be the first!";
          return;
        }
        table.innerHTML = content.join("");
      });
  }
  const table = document.querySelector(".table-content");
  fetchSubcribers();
});
