from django.conf.urls import include
from django.urls import path
from django.contrib import admin
from django.views.generic import TemplateView
from .views import index, email_exists, CreateAccountView, DeleteAccountView, AccountListView

urlpatterns = [
    path("email-exists", email_exists, name="email-exists"),
    path("create", CreateAccountView.as_view(), name="create-account"),
    path("delete", DeleteAccountView.as_view(), name="delete-account"),
    path("list", AccountListView.as_view(), name="list-account"),
    path("", index, name="list-account-html"),
]

