from django.test import TestCase, Client

# Create your tests here.
class ProfileTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_index(self):
        response = self.client.get("/profile/")
        self.assertTemplateUsed(response, "me/index.html")
        self.assertEqual(response.status_code, 200)

    def test_blog(self):
        response = self.client.get("/profile/posts")
        self.assertTemplateUsed(response, "me/blog_list.html")
        self.assertEqual(response.status_code, 200)
