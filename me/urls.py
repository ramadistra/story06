from django.urls import path
from . import views

# url for app, add your URL Configuration
app_name = "me"

urlpatterns = [path("posts", views.post_list, name="posts"), path("", views.index, name="index")]
