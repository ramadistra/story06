document.onreadystatechange = function() {
  if (document.readyState == "interactive") {
    document.querySelector("#spinner").remove();
  }
};

window.onload = function() {
  var themes = {
    blue: {
      accent: "#0085ff",
      background: "#2ea3f8",
      backgroundDark: "#2a7dca"
    },
    green: {
      accent: "green",
      background: "green",
      backgroundDark: "#0b540b"
    }
  };

  function setTheme(themeName) {
    localStorage.setItem("theme", themeName);
    renderTheme(themeName);
  }

  function renderTheme(themeName) {
    applyCSS(makeStyleSheet(themes[themeName]));
  }

  function applyCSS(styles) {
    Object.keys(styles).forEach(function(selector) {
      var style = styles[selector];
      Object.keys(style).forEach(function(attribute) {
        document.querySelectorAll(selector).forEach(function(elem) {
          elem.style[attribute] = style[attribute];
        });
      });
    });
  }

  function makeStyleSheet(theme) {
    return {
      ".content__Section__title, nav a": {
        color: theme.accent
      },
      ".sidebar": {
        backgroundColor: theme.background
      },
      footer: {
        backgroundColor: theme.backgroundDark
      }
    };
  }

  var appliedTheme = window.localStorage.getItem("theme") || "blue";
  var themeSelector = document.querySelector("#themeSelector");
  renderTheme(appliedTheme);
  themeSelector.value = appliedTheme;
  themeSelector.onchange = function() {
    setTheme(themeSelector.value);
  };

  document.querySelectorAll(".content__Section__title").forEach(function(elem) {
    elem.onclick = function() {
      this.parentNode.classList.toggle("active");
    };
  });
};
