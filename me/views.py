from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    return render(request, "me/index.html", {})


def post_list(request):
    return render(request, "me/blog_list.html", {})
