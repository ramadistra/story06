from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.
def index(request):
    form = StatusForm()
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("hello:index")

    status_list = Status.objects.all()
    context = {"form": form, "status_list": status_list}
    return render(request, "hello/index.html", context)
