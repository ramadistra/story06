from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class HelloFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("disable-gpu")
        self.driver = webdriver.Chrome("./chromedriver", chrome_options=chrome_options)

    def tearDown(self):
        self.driver.quit()

    def add_status(self, message):
        self.driver.get("http://127.0.0.1:8000")
        status = self.driver.find_element_by_id("id_message")
        status.send_keys(message)
        status.submit()

    def test_add_valid_status(self):
        self.add_status("Halo dunia")
        self.assertIn("Halo dunia", self.driver.execute_script("return document.body.innerHTML;"))

    def test_add_invalid_status(self):
        message = "a" * 305
        self.add_status(message)
        self.assertNotIn(message, self.driver.execute_script("return document.body.innerHTML;"))
