from django.test import TestCase, Client
from django import http
from .models import Status

# Create your tests here.
class HelloTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_valid_url(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_invalid_url(self):
        response = self.client.get("/thisisnotvalid")
        self.assertEqual(response.status_code, 404)

    def test_hello(self):
        response = self.client.get("/")
        self.assertIn(b"Hello, Apa kabar?", response.content)

    def test_create_status(self):
        self.client.post("/", {"message": "halo dunia"})
        self.assertEqual(Status.objects.count(), 1)

    def test_create_status_message_too_long(self):
        self.client.post("/", {"message": "halo dunia" * 100})
        self.assertEqual(Status.objects.count(), 0)

    def test_create_status_blank(self):
        self.client.post("/", {"message": ""})
        self.assertEqual(Status.objects.count(), 0)
