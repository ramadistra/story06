from django.db import models


class Status(models.Model):
    message = models.CharField(max_length=300)
    date_created = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["-date_created"]
