from django.conf.urls import include
from django.urls import path
from django.contrib import admin
from . import views

app_name = "hello"

urlpatterns = [path(r"", views.index, name="index")]

