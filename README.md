# Story 6 PPW 2018

1706027130 - Raihan Ramadistra Pratama

Pipeline status :
[![pipeline status](https://gitlab.com/ramadistra/story06/badges/master/pipeline.svg)](https://gitlab.com/ramadistra/story06/commits/master)

Code Coverage Status :
[![coverage report](https://gitlab.com/ramadistra/story06/badges/master/coverage.svg)](https://gitlab.com/ramadistra/story06/commits/master)

Link Heroku :
https://ppw18.herokuapp.com/
