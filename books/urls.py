from django.conf.urls import include
from django.urls import path
from django.contrib import admin
from django.views.generic import TemplateView
from .views import books_api, add, google_signin, logout, get_profile, delete, get

urlpatterns = [
    path("api/favorites/get", get, name="list-favorites"),  
    path("api/favorites/add", add, name="add-favorites"),  
    path("api/favorites/delete", delete, name="delete-favorites"),  
    path("api/profile/", get_profile, name="login-google"),  
    path("api/login-google/", google_signin, name="login-google"),  
    path("api/logout/", logout, name="logout"),  
    path("api", books_api, name="list-books"),
    path("", TemplateView.as_view(template_name="books/index.html"), name="index"),
]


