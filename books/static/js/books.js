function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}


window.onload = function() {  
  var favorites;
  function searchBooks(query) {
    fetch(`/books/api?q=${query}`)
      .then(response => response.json())
      .then(data =>
        (data.items || []).map(
          item => `
              <tr>
                  <td>${item.volumeInfo.title}</td>
                  <td>${
                    item.volumeInfo.authors
                      ? item.volumeInfo.authors.join(", ")
                      : "-"
                  }</td>
                  <td>${item.volumeInfo.publisher || "-"}</td>
                  <td><button data-bookid="${item.id}" class="fav-button ${(favorites.indexOf(item.id) >= 0) && 'fav-active'}"></button></td>
              </tr>`
        )
      )
      .then(content => {
        if (content.length < 1) {
          table.innerHTML = "No books found.";
          return;
        }
        table.innerHTML = content.join("");
        document.querySelectorAll(".fav-button").forEach(
          elm => 
            (elm.onclick = () => {
              const toggled = elm.classList.toggle("fav-active") | 0;
              count.innerText = (count.innerText | 0) + (toggled || -1);
              if (toggled) {
                  postData('/books/api/favorites/add', {id: elm.dataset.bookid})
                  favorites.push(elm.dataset.bookid)
              } else {
                postData('/books/api/favorites/delete', {id: elm.dataset.bookid})
                favorites.remove(elm.dataset.bookid)
              }
            })
        );
      });
  }

  const searchBox = document.getElementById("searchBox");
  const searchButton = document.getElementById("searchButton");
  const count = document.getElementById("favoriteCount");
  const table = document.querySelector(".table-content");
  searchButton.onclick = () => {
    table.innerHTML = "Loading...";
    searchBooks(searchBox.value);
  };
  fetch("/books/api/favorites/get").then(data => data.json()).then((data) => {
    favorites = data;
    count.innerText = favorites.length;
  }).then(() => searchButton.click())
};

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    postData("/books/api/login-google/", {
        id_token: googleUser.getAuthResponse().id_token,
        name: profile.getName(),
        image_url: profile.getImageUrl(),
    }).then((data) => data.success && fetch('/books/api/profile'))
    .then(data => data.json())
    .then(data => document.getElementById("username").innerText = data.name)
    .then(() => searchButton.click())
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(() => postData("/books/api/logout/"))
    .then(() => favorites = [])
    .then(() => searchButton.click());
  }

serialize = function(obj) {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

function postData(url = '', data = {}) {
  // Default options are marked with *
    return fetch(url, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-CSRFToken": getCookie("csrftoken"),
        },
        referrer: "no-referrer", // no-referrer, *client
        body: serialize(data), // body data type must match "Content-Type" header
    })
    .then(response => response.json()); // parses response to JSON
}