import requests
from django.http import JsonResponse
from django.views import View


def books_api(request):
    url = f'https://www.googleapis.com/books/v1/volumes?q={request.GET["q"]}'
    return JsonResponse(requests.get(url).json())


def get(request):
    return JsonResponse(request.session.get('books', []), safe=False)


def add(request):
    response = {"success": False}
    books = request.session['books'] = request.session.get('books', [])
    if request.method != "POST":
        response["message"] = "invalid method"
    elif not request.POST.get("id"):
        response["message"] = "please provide an id"
    else:
        books.append(request.POST["id"])
        response["success"] = True
    print(response)
    return JsonResponse(response)


def delete(request):
    response = {"success": False}
    books = request.session['books'] = request.session.get('books', [])
    if request.method != "POST":
        response["message"] = "invalid method"
    elif not request.POST.get("id"):
        response["message"] = "please provide an id"
    else:
        books.remove(request.POST["id"])
        response["success"] = True
    return JsonResponse(response)


def google_signin(request):
    if request.method != "POST":
        return JsonResponse({"success":False, "message":"Invalid method"})
    token = request.POST.get("id_token")
    if not token:
        return JsonResponse({"success":False, "message":"Invalid id_token"})
    # Security iz my pession <3
    request.session["id_token"] = token
    request.session["name"] = request.POST.get("name")
    request.session["image_url"] = request.POST.get("image_url")
    return JsonResponse({"success":True})


def logout(request):
    if request.method == "POST":
        request.session.flush()
        return JsonResponse({"success": True})
    return JsonResponse({"success":False, "message":"Invalid method"})


def get_profile(request):
    return JsonResponse({
        "name": request.session.get('name'),
        "image_url": request.session.get('image_url')
    })  